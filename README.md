# rock-paper-scissors

> Completele breakdown of the game rock paper scissors and how this project works.

## Javascript (The brain of the project)

1. First of we will first make variables for the button to be clicked and the score to be displayed.

2. Then we will make a function for the computer to choose a random number between 0 and 2. The numbers will be assigned to rock, paper and scissors respectively.

3. Then we will make a function for the game to be played. We will use the if statement to check if the player wins, loses or draws.

4. Then we will make a function for the game to be reset.

Visit the website [here](https://alzywelzy.github.io/rock-paper-scissors/)
